//
//  ContentView.swift
//  UrlImageViewDemo
//
//  Created by Oliver Bates on 02/03/2020.
//  Copyright © 2020 kAppuccino Apps Limited. All rights reserved.
//

import SwiftUI
import UrlImageView

struct ContentView: View {
    var body: some View {
        NavigationView {
            List {
                UrlImage(withUrl: URL(string: "https://d16lwq5o0fvd7.cloudfront.net/images/presets/category_page_normal/catalogue/products/mux72ba/mux72ba.jpg")!).aspectRatio(contentMode: .fit)
                    .frame(height: 50)
                UrlImage(withUrl: URL(string: "https://d16lwq5o0fvd7.cloudfront.net/images/presets/category_page_normal/catalogue/products/mux72ba/mux72ba.jpg")!).aspectRatio(contentMode: .fit)
                .frame(height: 50)
                UrlImage(withUrl: URL(string: "https://d16lwq5o0fvd7.cloudfront.net/images/presets/category_page_normal/catalogue/products/mux72ba/mux72ba.jpg")!).aspectRatio(contentMode: .fit)
                .frame(height: 50)
                UrlImage(withUrl: URL(string: "https://d16lwq5o0fvd7.cloudfront.net/images/presets/category_page_normal/catalogue/products/mux72ba/mux72ba.jpg")!).aspectRatio(contentMode: .fit)
                .frame(height: 50)
                
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
