//
//  UrlImageController.swift
//  UrlImageView
//
//  Created by Oliver Bates on 28/02/2020.
//  Copyright © 2020 kAppuccino Apps Limited. All rights reserved.
//

import Foundation
import SwiftUI
import UIKit

public class UrlImageController {
    
    public static func getImage(from url: URL,then completionHandler: @escaping ((_ image : UIImage?) -> Void)) {
        let tmpKey = MD5(url.absoluteString)
        let fileManager = FileManager()
        let tmpPath = "\(tmpKey)_icon.png"
        guard fileManager.fileExists(atPath: tmpPath) else {
            let request = URLRequest(url: url)
            let task = URLSession.shared.dataTask(with: request) { (data, _, error) in
                
                guard error == nil else {
                    completionHandler(nil)
                    return
                }
                guard let data = data else {
                    completionHandler(nil)
                    return
                }
                guard let image = UIImage(data: data) else {
                    completionHandler(nil)
                    return
                }
                guard let imageData = image.jpegData(compressionQuality: 0.2) else {
                    completionHandler(nil)
                    return
                }
                try? imageData.write(to: URL(fileURLWithPath: tmpPath))
                completionHandler(image)
            }
            task.resume()
          return
        }
        
        guard let image = UIImage(contentsOfFile: tmpPath) else {
            completionHandler(nil)
            return
        }

        completionHandler(image)
    }
}
