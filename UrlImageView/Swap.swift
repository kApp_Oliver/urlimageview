//
//  SwapView.swift
//  kAppuccino Apps Limited
//
//  Created by Oliver Bates on 10/07/2019.
//  Copyright © 2019 Jigsaw24. All rights reserved.
//

import SwiftUI

public struct Swap<C : View,D : View> : View {
    let this : (()->(C))
    let withThis : (()->(D))
    let condition : Bool
    
    public var body: some View {
        HStack {
            if !condition {
                this()
            }else{
                withThis()
            }
        }
    }
    
    public init(@ViewBuilder this: @escaping () -> C, @ViewBuilder withThis: @escaping () -> D, when : Bool) {
        self.this = this
        self.withThis = withThis
        self.condition = when
    }
}

#if DEBUG
struct SwapView_Previews : PreviewProvider {
    static var previews: some View {
        Swap(this: {
            HStack {
                Text("Yay")
                Text("Strange!")
            }
        }, withThis: {
            HStack {
                Text("Yay")
                Image(systemName: "clock")
                Text("Stdscsd")
            }
        },when: true)
    }
}
#endif
