//
//  MD5.swift
//  UrlImageView
//
//  Created by Oliver Bates on 28/02/2020.
//  Copyright © 2020 kAppuccino Apps Limited. All rights reserved.
//

import Foundation
import CryptoKit

func MD5(_ string: String) -> String {
    let digest = Insecure.MD5.hash(data: string.data(using: .utf8) ?? Data())

    return digest.map {
        String(format: "%02hhx", $0)
    }.joined()
}
