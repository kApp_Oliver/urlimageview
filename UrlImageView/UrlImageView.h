//
//  UrlImageView.h
//  UrlImageView
//
//  Created by Oliver Bates on 28/02/2020.
//  Copyright © 2020 kAppuccino Apps Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for UrlImageView.
FOUNDATION_EXPORT double UrlImageViewVersionNumber;

//! Project version string for UrlImageView.
FOUNDATION_EXPORT const unsigned char UrlImageViewVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <UrlImageView/PublicHeader.h>


