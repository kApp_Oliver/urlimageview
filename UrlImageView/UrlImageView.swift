//
//  UrlImageView.swift
//  UrlImageView
//
//  Created by Oliver Bates on 28/02/2020.
//  Copyright © 2020 kAppuccino Apps Limited. All rights reserved.
//
import Foundation
import SwiftUI

@available(iOS 13, *)
public struct UrlImage : View{
    
    public var url : URL
    @State var image : Image?
    @State var angle : Angle = Angle(degrees: 0.0)
    @State var renderV = 0.1
    
    var animation : Animation  {
        Animation.linear(duration: 1.0)
            .repeatForever(autoreverses: false)
    }
    
    public var body : some View {
        GeometryReader { proxy in
            Swap(this: {
                ZStack {
                    Circle()
                        .stroke(Color(red: 0.9,green: 0.9,blue: 0.9),lineWidth: proxy.size.width / 8)
                        .padding(proxy.size.width / 3)
                    Circle()
                        .trim(from: 0, to: 0.3)
                        .stroke(Color.orange,lineWidth: proxy.size.width / 8)
                        .padding(proxy.size.width / 3)
                        .rotationEffect(self.angle)
                        .onAppear {
                            
                            Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false) { (_) in
                                withAnimation(self.animation) {
                                    self.angle = Angle(degrees: 360.0)
                                }
                            }
                            
                    }
                    
                }
                .frame(width: proxy.size.width, height: proxy.size.height, alignment: .center)
            }, withThis: {
                self.image!
                    .renderingMode(.original)
                    .resizable()
                    .scaledToFit()
            }, when: self.image != nil)
        }
        .tag(self.renderV)
        .onAppear {
            UrlImageController.getImage(from: self.url) { (uiimage) in
                guard let uiimage = uiimage else {
                    return
                }
                DispatchQueue.main.async {
                    self.image = Image(uiImage: uiimage)
                    self.renderV += 0.01
                }
            }
        }
    }
    
    public init(withUrl url: URL){
        self.url = url
    }
}

struct UrlImageViewV_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            List {
                UrlImage(withUrl: URL(string: "https://d16lwq5o0fvd7.cloudfront.net/images/presets/product_swatch_thumbnail/catalogue/products/mwcc2ba/mwcc2ba.jpg")!).aspectRatio(contentMode: .fit)
                    .frame(height: 50)
            }
        }
        .previewDevice(PreviewDevice(rawValue: "iPhone 11 Pro"))
        .previewDisplayName("iPhone 11 Pro")
    }
}

