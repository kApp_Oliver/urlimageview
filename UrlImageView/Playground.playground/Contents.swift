import UIKit
import CryptoKit

func MD5(_ string: String) -> String {
    let digest = Insecure.MD5.hash(data: string.data(using: .utf8) ?? Data())
    return digest.map {
        String(format: "%02hhx", $0)
    }.joined()
}

var url = URL(string: "https://d16lwq5o0fvd7.cloudfront.net/images/presets/product_swatch_thumbnail/catalogue/products/mwcc2ba/mwcc2ba.jpg")!
print(MD5(url.absoluteString))
